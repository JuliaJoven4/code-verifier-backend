"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
/*Estamos llamando express a todo lo que nos hemos descargado de express */
const dotenv_1 = __importDefault(require("dotenv"));
/*Estamos llamando dotenv a todo lo que nos hemos descargado de dotenv */
//Configurate the .env file
//First step to use the .env file
dotenv_1.default.config();
//Creating Express App
//Create the app constant which saves an Express execution
/* Mientras que en js sería 'const app = express();', en TS podríamos tiparlo para que fuera: */
const app = (0, express_1.default)();
/* Mientras que en js sería 'const port = process.env.PORT || 8000;', en TS podríamos tiparlo para que fuera: */
//El | indica que puede ser un string o un number: cualquiera de las dos
const port = process.env.PORT || 8000;
//Define the first route of the APP
/*First parameter: string; second: function with a callback. It'll execute when it receives a petition to the
'/' route */
app.get('/', (req, res) => {
    //Send Hello World
    res.send('Welcome to API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose');
});
app.get('/hello', (req, res) => {
    //Send Hello World
    res.send('Welcome to GET Route: Hello!');
});
//Execute APP and Listen requests to PORT
app.listen(port, () => {
    /*Using those invert quoting marks allows the variable inside ${} to get its value */
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map